#!perl

use lib qw( libs/vendors );
use Encode;
use DB7;
use Getopt::Long;
use JSON;

use warnings;
use strict;

# Only for debuging
use Data::Dumper;

my $input;
my $output_dir = '';

GetOptions (
    "input=s"       => \$input,
    "output_dir=s"  => \$output_dir,
) or die( "Error in command line arguments\n");

use constant {
    DEBUG           => 0,

    TABLE_NAME_SUF  => '_xbase.dbf',
    FIELDS          => {
        ID  => [ 'F', 10, 1 ],
        MSG => [ 'C', 128 ],
    },

};


my $table_name = $output_dir . int( rand( 10 ) ) . '_' . time . TABLE_NAME_SUF;
$table_name = ( $output_dir . 'fixed_name' . TABLE_NAME_SUF ) if DEBUG;

unlink $table_name if -e $table_name;

my $db7 = new DB7(
    {
        'file'      => $table_name,
        'language'  => 'DBWINUS0',
        'code_page' => 0x01,
        'nocheck'   => 1,
    },
    FIELDS
);

die $db7->errstr if $db7->errstr;

my $data;
{
    local $/;
    open my $fh, "<:encoding(utf-8)", $input or die "Can't open file for reading: $!";
    my $json_text   = <$fh>;

    $data = eval{ decode_json( encode( 'utf8', $json_text ) ) };

    die "Can't parse JSON. Error: " . $@ if $@;

    close $fh;
}

my $values;

foreach my $entry ( @$data ) {
    foreach my $field ( keys FIELDS ) {
        $values->{ $field } = encode( 'cp1251', $entry->{ $field } ) || '';
    }

    $db7->add_record( $values );
}

die $db7->errstr if $db7->errstr;

$db7->write_file();

die $db7->errstr if $db7->errstr;

print( $table_name );
