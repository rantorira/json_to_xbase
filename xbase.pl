#!perl

use lib qw( libs/vendors );
use Encode;
use XBase;
use Getopt::Long;
use JSON;

use warnings;
use strict;

# Only for debuging
use Data::Dumper;

my $input;
my $output_dir = '';

GetOptions (
    "input=s"       => \$input,
    "output_dir=s"  => \$output_dir,
) or die( "Error in command line arguments\n");

use constant {
    DEBUG           => 0,

    TABLE_NAME_SUF  => '_xbase.dbf',
    FIELD_NAMES     => [ "ID", "MSG" ],
    FIELD_TYPES     => [ "N", "C" ],
    FIELD_LENGTHS   => [ 6, 40 ],
    FIELD_DECIMALS  => [ 2, undef ],
};


my $table_name = $output_dir . int( rand( 10 ) ) . '_' . time . TABLE_NAME_SUF;
$table_name = ( $output_dir . 'fixed_name' . TABLE_NAME_SUF ) if DEBUG;

unlink $table_name if -e $table_name;

my $table = new XBase( $table_name );

$table->drop() if defined $table;

$table = create XBase(
    'name'              => $table_name,
    'field_names'       => FIELD_NAMES,
    'field_types'       => FIELD_TYPES,
    'field_lengths'     => FIELD_LENGTHS,
    'field_decimals'    => FIELD_DECIMALS,
);

die XBase->errstr() if XBase->errstr();

unless( defined $table ) {
    die( "Can't create XBase table " . $table_name );
}

warn "DEBUG: Headers:\n" . $table->get_header_info() if DEBUG;

my $data;
{
    local $/;
    open my $fh, "<:encoding(utf-8)", $input or die "Can't open file for reading: $!";
    my $json_text   = <$fh>;

    $data = eval{ decode_json( encode( 'utf8', $json_text ) ) };

    die "Can't parse JSON. Error: " . $@ if $@;

    close $fh;
}

my $protected_xbase_counter = 0;
my $values;
my $fields = FIELD_NAMES;

foreach my $entry ( @$data ) {
    foreach my $field ( @$fields ) {
        $values->{ $field } = encode( 'cp1251', $entry->{ $field } ) || '';
    }

    $table->set_record_hash( $protected_xbase_counter, %$values ) or die $table->errstr();

    $protected_xbase_counter++;
}

$table->dump_records() if DEBUG;
$table->close();

print( $table_name );
