package DB7;
use strict;
use warnings;
use English qw /-no_match_vars/;

# ------------------------------------------------------------------------------
use vars qw /$VERSION/;
$VERSION = '1.2';

# ------------------------------------------------------------------------------
my $DB7_SIGNATURE    = 0x04;
my $DB7_HEADER_END   = 0x0D;
my $DB7_FILE_END     = 0x1A;
my $DB7_CHAR_MAX     = 255;
my $DB7_CHAR_BITS    = 8;
my $DB7_INT_MAX      = 2_147_483_647;
my $DB7_INT_MIN      = -2_147_483_648;
my $DB7_RECNAME_MAX  = 32;
my $DB7_RECORD_SIGN  = 32;               # 32 - regular, 42 - deleted
my $DB7_DATE_SIZE    = 8;
my $DB7_BOOL_SIZE    = 1;
my $DB7_INT_SIZE     = 4;
my $DB7_DEF_CODEPAGE = 0x03;
my $DB7_DEF_LANGUAGE = 'DBWINUS0';

my $DB7_HEADER = <<'EOL';
C       //  signature
C3      //  created, YMD
L       //  records number, as 32-bit unsigned
S       //  header size, as 16-bit unsigned
S       //  record size, as 16-bit unsigned
a17     //  reserved[2], dBase IV[2], multiuser[12], MDX[1],
C       //  dBase IV, Visual FoxPro, XBase codepage[1]
S       //  reserved[2]
a32     //  language driver
L       //  reserved[4]
EOL

my $DB7_HEADER_TPL = $DB7_HEADER;
$DB7_HEADER_TPL =~ s{\s+//.+$}{}gm;
$DB7_HEADER_TPL =~ s{\s+}{}g;
my $DB7_HEADER_SIZE = length( pack $DB7_HEADER_TPL, 0 );


my $DB7_FIELD_DESCR = <<'EOL';
a32     //  field name
a       //  field type[1]
C       //  field length, 1st byte
C       //  2nd byte of length (type=C) or 0
a13     //  reserved[2], MDX, reserved[2], autoincrement[int32], reserved[4]
EOL

my $DB7_FDESCR_TPL = $DB7_FIELD_DESCR;
$DB7_FDESCR_TPL =~ s{\s+//.+$}{}gm;
$DB7_FDESCR_TPL =~ s{\s+}{}g;
my $DB7_FDECSR_SIZE = length( pack $DB7_FDESCR_TPL, 0 );

# ------------------------------------------------------------------------------
sub new
{
    my ( $class, $opt, $vars ) = @_;

    my $self = bless $opt, $class;

    $self->{'vars'}        = $vars;
    $self->{'record_size'} = 0;
    $self->{'header_size'} = $DB7_HEADER_SIZE + 1;
    $self->{'records'}     = [];
    $self->{'error'}       = undef;

    $self->{'language'}  ||= $DB7_DEF_LANGUAGE;
    $self->{'code_page'} ||= $DB7_DEF_CODEPAGE;

    foreach my $key ( keys %{$vars} )
    {
        my $length = length $key;
        $self->{'error'} =
            "Invalid name length for '$key' ($length chars, 32 max)", last
            if $length > $DB7_RECNAME_MAX;

        $self->{'error'} = "Invalid type '$vars->{$key}->[0]' for '$key'", last
            if $vars->{$key}->[0] !~ /^[IDLCF]$/;

        $self->{'vars'}->{$key}->[1] = $DB7_DATE_SIZE
            if $vars->{$key}->[0] eq 'D';
        $self->{'vars'}->{$key}->[1] = $DB7_BOOL_SIZE
            if $vars->{$key}->[0] eq 'L';
        $self->{'vars'}->{$key}->[1] = $DB7_INT_SIZE
            if $vars->{$key}->[0] eq 'I';
        $self->{'record_size'} += $self->{'vars'}->{$key}->[1];
        $self->{'header_size'} += $DB7_FDECSR_SIZE;
    }

    return $self;
}

# ------------------------------------------------------------------------------
sub errstr
{
    my ( $self ) = @_;
    return $self->{'error'};
}

# ------------------------------------------------------------------------------
sub add_record
{
    my ( $self, $data ) = @_;

    return $self->{'error'} if $self->{'error'};

    my %rec;
    foreach my $name ( keys %{ $self->{'vars'} } )
    {
        my $value = $data->{$name};
        if( $value )
        {
            $self->_validate_value( $name, $value ) unless $self->{'nocheck'};
            return $self->{'error'} if $self->{'error'};
        }
        $rec{$name} = $value || '';
    }
    push @{ $self->{'records'} }, \%rec;
    return;
}

# ------------------------------------------------------------------------------
sub write_file
{
    my ( $self ) = @_;

    return $self->{'error'} if $self->{'error'};

    return $self->_e(
        'No fields description given in ' . __PACKAGE__ . '::new()' )
        if( !$self->{'vars'} || !keys %{ $self->{'vars'} } );

    open my $dbf, '>:raw', $self->{'file'}
        or
        return $self->_e( "Can not OPEN \"$self->{'file'}\": $ERRNO" );
    binmode $dbf;

    return $self->{'error'} if $self->_write_header( $dbf );

    foreach my $key ( sort keys %{ $self->{'vars'} } )
    {
        my $decimal = 0;
        if ( $self->{'vars'}->{$key}->[0] eq 'C' ) {
            $decimal = ( ( $self->{'vars'}->{$key}->[1] << $DB7_CHAR_BITS ) & $DB7_CHAR_MAX );
        }
        elsif ( $self->{'vars'}->{$key}->[0] eq 'F' ) {
            $decimal = $self->{'vars'}->{$key}->[2] || 2;
        }

        print {$dbf} pack(
            $DB7_FDESCR_TPL,
            $key,
            $self->{'vars'}->{$key}->[0],
            ( $self->{'vars'}->{$key}->[1] & $DB7_CHAR_MAX ),
            $decimal,
            '' );
    }
    print {$dbf} pack( 'C', $DB7_HEADER_END );

    foreach my $record ( @{ $self->{'records'} } )
    {
        print {$dbf} pack( 'C', $DB7_RECORD_SIGN );
        foreach my $key ( sort keys %{$record} )
        {
            if( $self->{'vars'}->{$key}->[0] eq 'I' )
            {
                print {$dbf} pack( 'l>', ( $record->{$key} || 0 ) );
            }
            else
            {
                print {$dbf} pack(
                    'A' . ( $self->{'vars'}->{$key}->[1] ),
                    $record->{$key} );
            }
        }
    }

    print {$dbf} pack( 'C', $DB7_FILE_END );
    close $dbf
        or return $self->_e(
        "Can not CLOSE \"$self->{'file'}\": $ERRNO" );

    return $self->{'error'};
}

# ------------------------------------------------------------------------------
sub _e
{
    my ( $self, $error ) = @_;
    $self->{'error'} = $error;
    return $self->{'error'};
}

# ------------------------------------------------------------------------------
sub _validate_value
{
    my ( $self, $name, $value ) = @_;

    if( $self->{'vars'}->{$name}->[0] eq 'I' )
    {
        if(    $value !~ /^[-+]?\d+$/
            || $value < $DB7_INT_MIN
            || $value > $DB7_INT_MAX )
        {
            return $self->_e( "Invalid INTEGER value of '$name': $value" );
        }
    }
    else
    {
        my $length = length $value;
        if( $length > $self->{'vars'}->{$name}->[1] )
        {
            return $self->_e( "Too long value for field '$name': $length/"
                    . $self->{'vars'}->{$name}->[1] );
        }
    }

    if( $self->{'vars'}->{$name}->[0] eq 'D' )
    {
        if( $value !~ /^\d{4}(\d\d)(\d\d)$/ || $1 > 12 || $2 > 31 )
        {
            return $self->_e( "Invalid DATE value for '$name': '$value'" );
        }
    }

    if(    $self->{'vars'}->{$name}->[0] eq 'L'
        && $value !~ /^[TYNF ?]$/i )
    {
        return $self->_e( "Invalid LOGICAL value for '$name': '$value'" );
    }
}

# ------------------------------------------------------------------------------
sub _write_header
{
    my ( $self, $dbf ) = @_;

    return $self->{'error'} if $self->{'error'};

    my ( undef, undef, undef, $mday, $mon, $year ) = gmtime( time );

    print {$dbf} pack(
        $DB7_HEADER_TPL,
        $DB7_SIGNATURE,
        $year, $mon + 1, $mday,
        scalar @{ $self->{'records'} },
        $self->{'header_size'},
        $self->{'record_size'},
        '',
        $self->{'code_page'},
        0,
        $self->{'language'},
        0 );

    return $self->{'error'};
}

# ------------------------------------------------------------------------------
1;

